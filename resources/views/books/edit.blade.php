@extends('layout')

@section('content')
    <style>
        .uper {
            margin-top: 40px;
        }
    </style>
    <div class="card uper">
        <div class="card-header">
            Edit Book
        </div>
        <div class="card-body">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
            @endif
            <form method="post" action="{{ route('books.update', $book->id) }}">
                @method('PATCH')
                @csrf
                <div class="form-group">
                    <label for="name">Book Name:</label>
                    <input type="text" class="form-control" name="name" value="{{ $book->name }}" />
                    <select name="author" class="form-control">
                        @foreach ($authors->all() as $author)
                            <option @if ($author->id == $book->author->id) selected @endif value="{{$author->id}}">
                                {{ $author->name }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">Update</button>
            </form>
        </div>
    </div>
@endsection