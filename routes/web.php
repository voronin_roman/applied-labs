<?php

Route::resources([
    'books' => 'BookController',
    'authors' => 'AuthorController',
]);

Route::view('/', 'welcome');