<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Book extends Model
{
    protected $fillable = ['name', 'author_id'];

    public function author()
    {
        return $this->belongsTo(Author::class);
    }

    public function scopeFilter($query, Request $request)
    {
        if ($name = $request->get('name')) {
            $query->where('name', 'like', "%{$name}%");
        }

        return $query;
    }
}
