###### Composer
 
```bash
sudo apt-get install curl
curl -O https://getcomposer.org/composer.phar
mv composer.phar composer
chmod +x composer
sudo mv composer /usr/local/bin
composer install
```

###### Config

- set DB_DATABASE with absolute path to applied_labs_test.db
- move .env.example to .env

###### node

```bash
npm install
npm run dev 
```

###### start server

```bash
php artisan serve
```